/*
 * Coach.java
 */
package carparksystem;

import javax.swing.ImageIcon;

/**
 * Vehicle subclass Coach, to store specific information and methods.
 * 
 * @author James Hudson
 */
public class Coach extends Vehicle
{
    private int numOfPassengers;
    private boolean touristOperator;
    
    
    /**
     * Default Constructor.
     */
    Coach()
    {
        super();
        
        numOfPassengers = 0;
        touristOperator = false;
    }
    
    
    /**
     * Constructor taking new coach details.
     * 
     * @param rNo   Registration number.
     * @param tour  Tour operator status.
     * @param pgr   Passenger capacity.
     */
    public Coach(String rNo, boolean tour, int pgr)
    {
        super(rNo);
        
        numOfPassengers = pgr;
        touristOperator = tour;
        
        calcCharge();
    }
    
    
    /**
     * Method to return the number of passengers.
     * 
     * @return  0 if 20 seats or less or 1 if more than 20 seats.
     */
    public int getNumOfPassengers()
    {
        return numOfPassengers;
    }
    
    
    /**
     * Method to return a description of the seating capacity.
     * 
     * @return  a string representation of the coach's seating capacity.
     */
    public String passengersString()
    {
        if (numOfPassengers == 1)
            return "Over 20 seats";
        return "Up to 20 seats";
    }
    
    
    /**
     * Method to return the tourist operator status.
     * 
     * @return  true if a tourist operator.
     */
    public boolean getTouristOperator()
    {
        return touristOperator;
    }
    
    
    /**
     * Method to return a description of the tourist operator status.
     * 
     * @return  a string representation of the tourist operator status.
     */
    public String touristString()
    {
        if (touristOperator)
            return "Company is a tour operator";
        return "Company is not a tour operator";
    }
    
    
    /**
     * Method to return the coach's details.
     * 
     * @return  an object array containing the details.
     */
    @Override
    public Object[] getCurrentData()
    {
        Object[] temp= {super.getRegNumber(), touristOperator, numOfPassengers, "n/a"};
        
        return temp;
    }
    
    
    /**
     * Method to return the vehicle type.
     * 
     * @return  Coach as a string.
     */
    @Override
    public String getType()
    {
        return "COACH";
    }
    
    
    /**
     * Method to test if the coach is within limits.
     * 
     * @return  always returns true as no test for a coach.
     */
    @Override
    public boolean testLimit()
    {
        return true;
    }
    
    
    
    /**
     * Method to get an instance of a coach image.
     * 
     * @return  ImageIcon instance containing the image to show on the GUI.
     */
    @Override
    public ImageIcon getImage()
    {
        return (new ImageIcon("ParkingImages/acoach.jpg"));
    }
    
    
    /**
     * Method to calculate the parking charge.
     */
    @Override
    public void calcCharge()
    {
        double discount = 1.0;
        
        if (touristOperator)
            discount = 0.9;
        
        if (numOfPassengers == 1)
            charge = (6 * discount);
        else
            charge = (4.5 * discount);
    }
    
    
    /**
     * Method to return the details of the coach.
     * 
     * @return  the description as a string.
     */
    @Override
    public String toString()
    {
        return ("Coach\n" + super.toString() + "\n" + passengersString() + " passengers.\n" + touristString());
    }
}
