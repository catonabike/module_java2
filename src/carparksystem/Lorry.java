/*
 * Lorry.java
 */
package carparksystem;

import javax.swing.ImageIcon;

/**
 * Vehicle subclass Lorry, to store specific information and methods.
 * 
 * @author James Hudson
 */
public class Lorry extends Vehicle
{
    private double weight;
    private int numOfDays;
    
    
    /**
     * Default Constructor.
     */
    Lorry()
    {
        super();
        
        weight = 0.0;
        numOfDays = 0;
    }
    
    
    /**
     * Constructor taking new lorry details.
     * 
     * @param rNo   Registration number.
     * @param ton   Lorry weight.
     * @param day   Parking time in days.
     */
    public Lorry(String rNo, double ton, int day)
    {
        super(rNo);
        
        numOfDays = day;
        
        setWeight(ton);
        
        calcCharge();
    }
    
    
    /**
     * Method to return the lorry weight.
     * 
     * @return  the weight in tonnes as a double.
     */
    public double getWeight()
    {
        return weight;
    }
    
    
    /**
     * Method to set lorry weight.
     * 
     * @param ton Lorry weight.
     */
    public void setWeight(double ton)
    {
        weight = ton;
        
        if (weight < 7.5)
            weight = 7.5;
    }
    
    
    /**
     * Method to return the lorries details.
     * 
     * @return  an object array containing the details.
     */
    @Override
    public Object[] getCurrentData()
    {
        Object[] temp= {super.getRegNumber(), weight, numOfDays, "n/a"};
        
        return temp;
    }
    
    
    /**
     * Method to return the vehicle type.
     * 
     * @return  Lorry as a string.
     */
    @Override
    public String getType()
    {
        return "LORRY";
    }
    
    
    /**
     * Method to test if the lorries weight is under the limit.
     * 
     * @return  true if less then or equal to 35 tonnes.
     */
    @Override
    public boolean testLimit()
    {
        return weight <= 35;
    }
    
    
    /**
     * Method to get an instance of a lorry image.
     * 
     * @return  ImageIcon instance containing the image to show on the GUI.
     */
    @Override
    public ImageIcon getImage()
    {
        return (new ImageIcon("ParkingImages/alorry.jpg"));
    }
    
    
    /**
     * Method to calculate the parking charge.
     */
    @Override
    public void calcCharge()
    {
        if (weight < 20.0)
            charge = (5 * numOfDays);
        else
            charge = (8 * numOfDays);
    }
    
    
    /**
     * Method to return the details of the lorry.
     * 
     * @return  the description as a string.
     */
    @Override
    public String toString()
    {
        return ("Lorry\n" + super.toString() + "\nWeight: " + String.format("%.2f", weight) + " tonnes.\nStay duration: " + numOfDays + " days.");
    }
}
