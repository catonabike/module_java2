/*
 * VehicleData.java
 */
package carparksystem;

import java.io.IOException;
import java.util.HashMap;
import javax.swing.ImageIcon;

/**
 * Class holding the HashMap of vehicles, representing the car park, and car park data.
 * 
 * @author James hudson
 */
public class VehicleData
{
    private HashMap<Integer, Vehicle> carPark;
    private final int largeSpaces, smallSpaces;
    private double deletedTotal; // total charges of vehicles deleted (using right click) from car park
    
    
    /**
     * Default Constructor.
     */
    VehicleData()
    {
        carPark = new HashMap<>();
        
        largeSpaces = 4;
        smallSpaces = 12;
        deletedTotal = 0.0;
    }
    
    
    /**
     * Constructor for the specified size of car park.
     * 
     * @param l Number of large spaces.
     * @param s Number of small spaces.
     */
    public VehicleData(int l, int s)
    {
        carPark = new HashMap<>();
        
        largeSpaces = l;
        smallSpaces = s;
        deletedTotal = 0.0;
    }
    
    
    /**
     * Method to return the number of large spaces.
     * 
     * @return  Value as an integer.
     */
    public int getLargeSpaces()
    {
        return largeSpaces;
    }
    
    
    /**
     * Method to return the number of large spaces.
     * 
     * @return   Value as an integer.
     */
    public int getSmallSpaces()
    {
        return smallSpaces;
    }
    
    
    /**
     * Method to test if all of the keys allocated as large spaces are present in the HashMap, 
     * and return the first available space if not.
     * 
     * @return First available space as an integer(0 base), or -1 if there are no free spaces.
     */
    public int testLargeSpace()
    {
        for (int fs = 0; fs < largeSpaces; fs++)
        {
            if (!carPark.containsKey(fs))
                return fs;
        }
        return -1;
    }
    
    
    /**
     * Method to test if all of the keys allocated as small spaces are present in the HashMap, 
     * and to return the first available key if not.
     * 
     * @return First available space as an integer(0 base), or -1 if there are no free spaces.
     */
    public int testSmallSpace()
    {
        for (int fs = largeSpaces; fs < (largeSpaces + smallSpaces); fs++)
        {
            if (!carPark.containsKey(fs))
                return fs;
        }
        return -1;
    }
    
    
    /**
     * Method to test if the key is present, showing the space is full.
     * 
     * @param k Space to test for(0 base).
     * 
     * @return  true if the key is present.
     */
    public boolean spaceFull(int k)
    {
        return carPark.containsKey(k);
    }
    
    
    /**
     * Method to clear all vehicles from the car park and reset the total charges of previous vehicles to 0.
     */
    public void resetSpaces()
    {
        carPark.clear();
        
        deletedTotal = 0.0;
    }
    
    
    /**
     * Method to add a vehicle to the car park.
     * 
     * @param k Space number(0 base).
     * @param v Vehicle to add.
     */
    public void insertVehicle(int k, Vehicle v)
    {
        carPark.put(k, v);
    }
    
    
    /**
     * Method to remove a vehicle from the car park and increase the total of past charges.
     * 
     * @param k Space containing the vehicle(0 base).
     */
    public void deleteVehicle(int k)
    {
        deletedTotal += carPark.get(k).getCharge();
        
        carPark.remove(k);
    }
    
    
    /**
     * Method to create an instance of the CarParkFile class to serialise the car park.
     * 
     * @param name  File name to save the data to.
     * 
     * @return  true if the data is serialised successfully.
     * 
     * @throws java.io.IOException if the specified file can not be created or opened or written to.
     */
    public boolean savePark(String name) throws IOException
    {
        return new CarParkFile(name, carPark).savePark();
    }
    
    
    /**
     * Method to create an instance of the CarParkFile class to de-serialise the car park.
     * 
     * @param name  File name to read the data from.
     * 
     * @return  true if a new car park is created.
     * 
     * @throws java.lang.ClassNotFoundException if the class of the serialised object is not specified.
     * @throws java.io.IOException if the specified file can not be opened or read from.
     */
    public boolean loadPark(String name) throws ClassNotFoundException, IOException
    {
        HashMap<Integer, Vehicle> tempPark = new CarParkFile(name, carPark).loadPark();
        
        if (tempPark != null)
        {
            carPark = tempPark;
            
            return true;
        }
        return false;
    }
    
    
    /**
     * Method to calculate the total charges of all of the vehicles currently in the car park.
     * 
     * @return  Total as a double.
     */
    public double currentTotal()
    {
        double currentTotal = 0.0;
        
        for (int i = 0; i < (largeSpaces + smallSpaces); i++)
        {
            if (carPark.containsKey(i))
                currentTotal += carPark.get(i).getCharge();
        }
        return currentTotal;
    }
    
    
    /**
     * Method to convert the vehicle parking charge to a formatted String to display.
     * 
     * @param k Space containing the vehicle(0 base).
     * 
     * @return  Current charge as a formatted string.
     */
    public String getVehicleCharge(int k)
    {
        return String.format("%.2f", carPark.get(k).getCharge());
    }
    
    
    /**
     * Method to convert the current total a formatted String to display.
     * 
     * @return  Current total as a formatted string.
     */
    public String getCurrentTotal()
    {
        return String.format("%.2f", currentTotal());
    }
    
    
    /**
     * Method to calculate the total charges received over the day.
     * 
     * @return  Day total as a formatted string.
     */
    public String getDayTotal()
    {
        double dayTotal = currentTotal() + deletedTotal;
        
        return String.format("%.2f", dayTotal);
    }
    
    
    /**
     * Method to get a description of a vehicle in the car park.
     * 
     * @param k Space containing the vehicle(0 base).
     * 
     * @return  Vehicle description as a string.
     */
    public String getDescription(int k)
    {
        return carPark.get(k).toString();
    }
    
    
    /**
     * Method to get all of the current details of a vehicle.
     * 
     * @param k Space containing the vehicle(0 base).
     * 
     * @return  An array of objects containing the vehicle information.
     */
    public Object[] editVehicle(int k)
    {
        return carPark.get(k).getCurrentData();
    }
    
    
    /**
     * Method to get the type of a vehicle.
     * 
     * @param k Space containing the vehicle(0 base).
     * 
     * @return  String descriptor of the vehicle.
     */
    public String getVehicleType(int k)
    {
        return carPark.get(k).getType();
    }
    
    
    /**
     * Method to get an instance of the image for a vehicle.
     * 
     * @param k Space containing the vehicle(0 base).
     * 
     * @return  ImageIcon instance containing the image to show on the GUI.
     */
    public ImageIcon fetchImage(int k)
    {
        return carPark.get(k).getImage();
    }
}
