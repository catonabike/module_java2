/*
 * CarParkSystem.java
 */
package carparksystem;

import javax.swing.JFrame;

/**
 * Driver class for the Car Park System.
 * 
 * @author James Hudson
 */
public class CarParkSystem
{

    /**
     * Method creates an instance of the VehicleData class representing the car park and the main program GUI.
     * 
     * @param   args    Command line arguments.
     */
    public static void main(String[] args)
    {
        VehicleData vd = new VehicleData(4, 12);
        
        JFrame base = new JFrame("Hudson car parks");
        
        Overview mainGui = new Overview(vd);
        
        base.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        base.setLocation(30, 100);
        base.setVisible(true);
        base.add(mainGui);
        base.pack();
    }
}
