/*
 * Vehicle.java
 */
package carparksystem;

import java.io.Serializable;
import javax.swing.ImageIcon;

/**
 * Abstract super class of the Vehicle type subclasses with common data and methods.
 * 
 * @author  James Hudson
 * 
 * @serial  Objects of the Vehicle class and it's sub-classes are serialisable.
 */
public abstract class Vehicle implements Serializable
{
    protected double charge;
    protected String regNumber;
    
    
    /**
     * Default Constructor.
     */
    Vehicle()
    {
        charge = 0.0;
        regNumber = null;
    }
    
    
    /**
     * Constructor for a new vehicle.
     * 
     * @param regIn Registration number.
     */
    Vehicle(String regIn)
    {
        charge = 0.0;
        
        setRegNumber(regIn);
    }
    
    
    /**
     * Method to return the calculated parking charge for the vehicle.
     * 
     * @return  the charge as a double.
     */
    double getCharge()
    {
        return charge;
    }
    
    
    /**
     * Method to return the vehicle registration number.
     * 
     * @return  Registration number as a String.
     */
    String getRegNumber()
    {
        return regNumber;
    }
    
    
    /**
     * Method to set the vehicle registration number.
     * 
     * @param r Registration number.
     */
    void setRegNumber(String r)
    {
        regNumber = r.toUpperCase().trim();
    }
    
    
    /**
     * Method to test the vehicle registration number.
     * 
     * @return  true string is greater than 1 character and contains only letters and numbers.
     */
    boolean testRegNumber()
    {
        boolean rOk = true;
        
        if ((regNumber.length() < 1) || (regNumber.length() > 7))
        {
            rOk = false;
        }
        
        char[] ofR = regNumber.toCharArray();
        
        for (char c: ofR)
        {
            if (((c < '0') || (c > '9')) && ((c < 'A') || (c > 'Z')))
                rOk = false;
        }
        return rOk;
    }
    
    
    /**
     * Abstract method to return the vehicle image.
     * 
     * @return  an ImageIcon containing the vehicle image.
     */
    abstract ImageIcon getImage();
    
    
    /**
     * Abstract method to calculate the parking charge.
     */
    abstract void calcCharge();
    
    
    /**
     * Abstract method to return the vehicle details.
     * 
     * @return  an object array containing the vehicle details.
     */
    abstract Object[] getCurrentData();
    
    
    /**
     * Abstract method to return the vehicle type.
     * 
     * @return  the vehicle type as a string.
     */
    abstract String getType();
    
    
    /**
     * Abstract method to test the vehicles limiting factor for the car park.
     * 
     * @return true if limits are within the limits.
     */
    abstract boolean testLimit();
    
    
    /**
     * Method to return the vehicle description.
     * 
     * @return  the vehicle description as a string.
     */
    @Override
    public String toString()
    {
        return ("Registration Number: " + regNumber + "\nParking Charge: £" + String.format("%.2f", charge));
    }
}
