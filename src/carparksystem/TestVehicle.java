/*
 * TestVehicle.java
 */
package carparksystem;

/**
 * Class to test the function of the abstract vehicle class.
 * 
 * @author James
 */
class TestVehicle
{

    /**
     * Test class used to test the function of the Vehicle class.
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        /* 
        To test the Vehicle class, 1- Made the class a concrete class again
                                   2- Added a return to getCharge() and made a concrete method to test
                                   3- Created a Vehicle object in the TestVehicle class and tested the outputs
        See results in Test documentation.
        */
        
        /* COMMENTED OUT DURING NORMAL FUNCTION
        
        Vehicle v = new Vehicle();
        
        System.out.println();
        System.out.println(v.getCharge());
        System.out.println();
        System.out.println(v.calcCharge());
        System.out.println();
        System.out.println(v.toString());
        System.out.println();
        
        Vehicle vr = new Vehicle("M567HUT");
        
        System.out.println(vr.getCharge());
        System.out.println();
        System.out.println(vr.calcCharge());
        System.out.println();
        System.out.println(vr.toString());
        System.out.println();
        */
    }
    
}
