/*
 * TestCar.java
 */
package carparksystem;

/**
 * Class to test the function of the Car class.
 * 
 * @author James Hudson
 */
class TestCar
{
    
    /**
     * Test class used to test the function of the Car class.
     * 
     * @param args the command line arguments
     */
    
    public static void main(String[] args)
    {
        // See results in test documentation!
        
        Car c = new Car();
        
        System.out.println();
        System.out.println("*Empty Constructor*");
        System.out.println("Charge: " + c.getCharge());
        System.out.println("Length: " + c.getLength());
        System.out.println("Image location: " + c.getImage());
        System.out.println("The toString() output: " + c.toString());
        
        Car cr = new Car("M567HUT", 5.5, false, 3);
        
        System.out.println();
        System.out.println("*Constructor far a 5.5 metre long car without a disabled badge and parking for 3 hours. Car reg is M567HUT*");
        System.out.println("Charge: " + cr.getCharge());
        System.out.println("Length: " + cr.getLength());
        System.out.println("Image location: " + cr.getImage());
        System.out.println("The toString() output: " + cr.toString());
        
        Car cs = new Car("M567HUT", 5.5, true, 3);
        
        System.out.println();
        System.out.println("*Constructor far a 5.5 metre long car with a disabled badge and parking for 3 hours. Car reg is X567HUT*");
        System.out.println("Charge: " + cs.getCharge());
        System.out.println("Length: " + cs.getLength());
        System.out.println("Image location: " + cs.getImage());
        System.out.println("The toString() output: " + cs.toString());
        
        Car ct = new Car("NU65JUN", 6.1, false, 14);
        
        System.out.println();
        System.out.println("*Constructor far a 6.1 metre long car without a disabled badge and parking for 14 hours. Car reg is NU65JUN*");
        System.out.println("Charge: " + ct.getCharge());
        System.out.println("Length: " + ct.getLength());
        System.out.println("Image location: " + ct.getImage());
        System.out.println("The toString() output: " + ct.toString());
    }
}
