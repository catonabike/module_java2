/*
 * TestCoach.java
 */
package carparksystem;

/**
 * Class to test the functions of the Coach class.
 * 
 * @author James Hudson
 */
class TestCoach
{
    
    /**
     * Test class used to test the function of the Coach class.
     * 
     * @param args the command line arguments
     */
    
    public static void main(String[] args)
    {
        // See results in test documentation!
        
        Coach c = new Coach();
        
        System.out.println();
        System.out.println("*Empty Constructor*");
        System.out.println("Charge: " + c.getCharge());
        System.out.println("Passengers: " + c.getNumOfPassengers());
        System.out.println("Image location: " + c.getImage());
        System.out.println("The toString() output: " + c.toString());
        
        Coach cr = new Coach("CR1", false, 0);
        
        System.out.println();
        System.out.println("*Constructor far a 20 seater coach who is not a tour operator. Coach reg is CR1*");
        System.out.println("Charge: " + cr.getCharge());
        System.out.println("Passengers: " + cr.getNumOfPassengers());
        System.out.println("Image location: " + cr.getImage());
        System.out.println("The toString() output: " + cr.toString());
        
        Coach cs = new Coach("MM17SOS", true, 1);
        
        System.out.println();
        System.out.println("*Constructor far a 21 seater coach who is a tour operator. Coach reg is MM17SOS*");
        System.out.println("Charge: " + cs.getCharge());
        System.out.println("Passengers: " + cs.getNumOfPassengers());
        System.out.println("Image location: " + cs.getImage());
        System.out.println("The toString() output: " + cs.toString());
    }
}
