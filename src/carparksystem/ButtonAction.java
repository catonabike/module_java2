/*
 * ButtonAction.java
 */
package carparksystem;

import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 * Helper class to carry out the actions of the menu buttons and mouse actions on the Overview GUI.
 * 
 * @author James Hudson
 */
class ButtonAction
{
    private VehicleData parkLot;
    private Overview main;
    private JFrame inputBase;
    private int space;
    
    
    /**
     * Default Constructor.
     */
    ButtonAction()
    {}
    
    
    /**
     * Constructor for mouse event action.
     * 
     * @param vd    Vehicle data.
     * @param s     Selected space.
     * @param c     Mouse button clicked.
     * @param w     Overview GUI.
     */
    ButtonAction(VehicleData vd, int s, int c, Overview w)
    {
        parkLot = vd;
        main = w;
        space = s;
        
        switch(c)
        {
            case 1:
                showVehicle();
                break;
            case 2:
                editVehicleData();
                break;
            default:
                removeVehicle();
        }
    }
    
    
    /**
     * Constructor for a button event action.
     * 
     * @param vd    Vehicle data.
     * @param b     Button clicked on GUI.
     * @param w     Overview GUI.
     */
    ButtonAction(VehicleData vd, int b, Overview w)
    {
        parkLot = vd;
        main = w;
        
        switch(b)
        {
            case 0:
                addVehicle("CAR");
                break;
            case 1:
                addVehicle("LORRY");
                break;
            case 2:
                addVehicle("COACH");
                break;
            case 3:
                clearAll();
                break;
            case 4:
                saveVehicles();
                break;
            case 5:
                loadVehicles();
                break;
            case 6:
                outputCurrent();
                break;
            default:
                outputDay();
        }
    }
    
    
    /**
     * Method to test if the vehicle can be added to the car park and display message to the user if not.
     * 
     * @param d Type of vehicle.
     */
    private void addVehicle(String d)
    {
        if (d.equals("CAR"))
            space = parkLot.testSmallSpace();
        else
            space = parkLot.testLargeSpace();
        
        if (space > -1)
            getVehicleInfo(new Input(d, this));
        else
            JOptionPane.showMessageDialog(null, "No available spaces!", "Car Park information", JOptionPane.PLAIN_MESSAGE);
    }
    
    
    /**
     * Method to create a new instance of the Input GUI to get a new vehicles details.
     * 
     * @param i a new Input GUI instance.
     */
    private void getVehicleInfo(Input i)
    {
        inputBase = new JFrame("Vehicle details");
        
        Input inputGui = i;
        
        inputBase.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        inputBase.setLocation(475, 200);
        inputBase.setVisible(true);
        inputBase.add(inputGui);
        inputBase.pack();
    }
    
    
    /**
     * Method to display details of a vehicles if in the selected car park space and display message to the user if not.
     */
    private void showVehicle()
    {
        if (parkLot.spaceFull(space))
            JOptionPane.showMessageDialog(null, parkLot.getDescription(space), "Vehicle information", JOptionPane.PLAIN_MESSAGE);
        else
            JOptionPane.showMessageDialog(null, "This space is currently empty", "Car Park information", JOptionPane.PLAIN_MESSAGE);
    }
    
    
    /**
     * Method to create an instance of the Input GUI to edit a vehicles details if present in the selected car park space 
     * and display message to the user if not.
     */
    private void editVehicleData()
    {
        if (parkLot.spaceFull(space))
            getVehicleInfo(new Input(parkLot.getVehicleType(space), parkLot.editVehicle(space), this));
        else
            JOptionPane.showMessageDialog(null, "This space is currently empty", "Car Park information", JOptionPane.PLAIN_MESSAGE);
    }
    
    
    /**
     * Method to remove a vehicle from the car park if present in the selected car park space and display message to the user if not.
     */
    private void removeVehicle()
    {
        if (parkLot.spaceFull(space))
        {
            if (JOptionPane.showConfirmDialog(null, "Are you sure you want to remove this vehicle from the Car Park?", "Delete Warning", 
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null) == JOptionPane.YES_OPTION)
            {
                parkLot.deleteVehicle(space);
                
                main.resetImages();
            }
        }
        else
        {
            JOptionPane.showMessageDialog(null, "This space is currently empty", "Car Park information", JOptionPane.PLAIN_MESSAGE);
        }
    }
    
    
    /**
     * Method to create a new vehicle and add to the car park.
     * 
     * @param t     Type of vehicle.
     * @param ai    Vehicle details.    
     */
    void setNewData(String t, Object[] ai)
    {
        Vehicle temp;
        
        switch (t)
        {
            case "CAR":
                temp = new Car((String) ai[0], (Double) ai[1], (Boolean)  ai[2], (Integer)  ai[3]);
                break;
            case "LORRY":
                temp = new Lorry((String) ai[0], (Double) ai[1], (Integer) ai[2]);
                break;
            default:
                temp = new Coach((String) ai[0], (Boolean) ai[1], (Integer) ai[2]);
        }
        
        if (testNewData(temp))
        {
            parkLot.insertVehicle(space, temp);
            
            main.resetImages();
            
            JOptionPane.showMessageDialog(null, "The parking charge for the " + parkLot.getVehicleType(space) + " is: £" + 
                    parkLot.getVehicleCharge(space), "Car Park information", JOptionPane.PLAIN_MESSAGE);
        }
    }
    
    
    /**
     * Method to test the details of the newly created vehicle.
     * 
     * @param v Vehicle to test.
     * 
     * @return  true if details are within limits. 
     */
    boolean testNewData(Vehicle v)
    {
        if (v.testLimit() && v.testRegNumber())
        {
            return true;
        }
        else if (!v.testLimit() && v.getType().equals("CAR"))
        {
            JOptionPane.showMessageDialog(null, "There is a problem with the Car's length entry,\nthe length limit is 10 meters!", "Input information", JOptionPane.WARNING_MESSAGE);
            
            return false;
        }
        else if (!v.testLimit())
        {
            JOptionPane.showMessageDialog(null, "There is a problem with the lorries weight entry,\nthe weight limit is 35 tonnes!", "Input information", JOptionPane.WARNING_MESSAGE);
            
            return false;
        }
        else
        {
            JOptionPane.showMessageDialog(null, "There is a problem with the registration entry,\nit must be between 1 & 8 characters long\nand only contain letters and numbers", 
                    "Input information", JOptionPane.WARNING_MESSAGE);
            
            return false;
        }
    }
    
    
    /**
     * Method to dispose of the Input GUI.
     */
    void closeInput()
    {
        inputBase.dispose();
    }
    
    
    /**
     * Method to confirm user intention to clear all vehicles from the car park.
     */
    private void clearAll()
    {
        if (JOptionPane.showConfirmDialog(null, "Are you sure you want to clear ALL vehicles from the Car Park?", "Clear All Warning", 
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null) == JOptionPane.YES_OPTION)
        {
            parkLot.resetSpaces();
            
            main.resetImages();
        }
    }
    
    
    /**
     * Method to display the outcome of the save details request to the user.
     */
    private void saveVehicles()
    {     
        String ip = JOptionPane.showInputDialog(null, "Enter a file name to save the information to:\n( File must have a .dat extension! )", "File Information", JOptionPane.QUESTION_MESSAGE);
        
        if (ip != null)
        {
            try
            {
                if (parkLot.savePark(ip))
                    JOptionPane.showMessageDialog(null, "Data saved successfully", "File information", JOptionPane.PLAIN_MESSAGE);
                else
                    JOptionPane.showMessageDialog(null, "There was an problem with the file name!\nThe file name must be atleast 1 character and have a .dat extension", "File information", JOptionPane.WARNING_MESSAGE);
            }
            catch (IOException ex)
            {
                JOptionPane.showMessageDialog(null, "There was an error saving the file!\nCheck the file path and if the file exists", "File information", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    
    /**
     * Method to display the outcome of the load details request to the user.
     */
    private void loadVehicles()
    {
        String ip = JOptionPane.showInputDialog(null, "Enter a file name to load the information from:\n( File must have a .dat extension )", "File Information", JOptionPane.QUESTION_MESSAGE);
        
        if (ip != null)
        {
            try
            {
                if (parkLot.loadPark(ip))
                {
                    main.resetImages();
                    
                    JOptionPane.showMessageDialog(null, "Data loaded successfully", "File information", JOptionPane.PLAIN_MESSAGE);
                }
                else
                {
                    JOptionPane.showMessageDialog(null, "There was an problem with the file name!\nThe file name must be atleast 1 character and have a .dat extension", "File information", JOptionPane.WARNING_MESSAGE);
                }
            }
            catch (ClassNotFoundException | IOException ex)
            {
                JOptionPane.showMessageDialog(null, "There was an error loading the file!\nCheck the file path and if the file exists", "File information", JOptionPane.WARNING_MESSAGE);
            }
        }
    }
    
    
    /**
     * Method to display the current total to the user.
     */
    private void outputCurrent()
    {
        JOptionPane.showMessageDialog(null, "The current total is £" + parkLot.getCurrentTotal(), "Car Park information", JOptionPane.PLAIN_MESSAGE);
    }
    
    
    /**
     * Method to display the day total to the user.
     */
    private void outputDay()
    {
        JOptionPane.showMessageDialog(null, "The day total is £" + parkLot.getDayTotal(), "Car Park information", JOptionPane.PLAIN_MESSAGE);
    }
}