/*
 * TestLorry.java
 */
package carparksystem;

/**
 * Class to test the function of the Lorry class.
 * 
 * @author James Hudson
 */
class TestLorry
{
    
    /**
     * Test class used to test the function of the Lorry class.
     * 
     * @param args the command line arguments
     */
    
    public static void main(String[] args)
    {
        // See results in test documentation!
        
        Lorry l = new Lorry();
        
        System.out.println();
        System.out.println("*Empty Constructor*");
        System.out.println("Charge: " + l.getCharge());
        System.out.println("Weight: " + l.getWeight());
        System.out.println("Image location: " + l.getImage());
        System.out.println("The toString() output: " + l.toString());
        
        Lorry lr = new Lorry("X9MAY", 19.0, 3);
        
        System.out.println();
        System.out.println("*Constructor far a 19 tonne Lorry parking for 3 days. Lorry reg is X9MAY*");
        System.out.println("Charge: " + lr.getCharge());
        System.out.println("Weight: " + lr.getWeight());
        System.out.println("Image location: " + lr.getImage());
        System.out.println("The toString() output: " + lr.toString());
        
        Lorry ls = new Lorry("MM17VDP", 20.0, 1);
        
        System.out.println();
        System.out.println("*Constructor far a 20 tonne Lorry parking for 1 day. Lorry reg is MM17VDP*");
        System.out.println("Charge: " + ls.getCharge());
        System.out.println("Weight: " + ls.getWeight());
        System.out.println("Image location: " + ls.getImage());
        System.out.println("The toString() output: " + ls.toString());
    }
}
