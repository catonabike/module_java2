/*
 * Overview.java
 */
package carparksystem;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Class to build the GUI displaying an overview of the car park and controls for user actions.
 * 
 * @author James Hudson
 */
public class Overview extends JPanel implements ActionListener, MouseListener
{
    private VehicleData cPark;
    private int lSpaces, sSpaces;
    private JButton[] command;
    private JLabel[] space;
    private JPanel theButtons, theSpaces, lorries, cars;
    private String[] sNums;
    
    
    /**
     * Default Constructor.
     */
    Overview()
    {
        add(new JLabel("A data structure representing the car park is required to build the GUI"));
    }
    
    
    /**
     * Constructor for the car park control GUI.
     * 
     * @param vd    Car park data.
     */
    public Overview(VehicleData vd)
    {
        cPark = vd;
        lSpaces = cPark.getLargeSpaces();
        sSpaces = cPark.getSmallSpaces();
        
        sNums = new String[lSpaces + sSpaces];
        
        for (int n = 0; n < (lSpaces + sSpaces); n++)
        {
            sNums[n] = "ParkingImages/nums/s" + (n + 1) + ".jpg";
        }
        
        buildButtons();
        buildSpaces();
        
        setOpaque(true);
        setPreferredSize(new Dimension(1280, 545));
        setLayout(new BorderLayout());
        add(theButtons, BorderLayout.SOUTH);
        add(theSpaces, BorderLayout.CENTER);
    }
    
    
    /**
     * Method to construct a JPanel containing the interface buttons.
     */
    private void buildButtons()
    {
        theButtons = new JPanel();
        theButtons.setLayout(new GridLayout(1, 8));
        theButtons.setOpaque(true);
        theButtons.setPreferredSize(new Dimension(1280, 50));
        
        command = new JButton[8];
        
        String[] names = {"Add Car", "Add Lorry", "Add Coach", "Clear All", "Save", "Load", "Current total", "Total for day"};
        
        for (int b = 0; b < 8; b++)
        {
            command[b] = new JButton(names[b]);
            command[b].setPreferredSize(new Dimension(160, 50));
            command[b].addActionListener(this);
            
            theButtons.add(command[b]);
        }
    }
    
    
    /**
     * Method to construct the JPanel showing the car park spaces.
     */
    private void buildSpaces()
    {
        theSpaces = new JPanel();
        theSpaces.setOpaque(true);
        theSpaces.setPreferredSize(new Dimension(1280, 480));
        theSpaces.setBackground(Color.yellow);
        
        lorries = new JPanel();
        lorries.setLayout(new GridLayout(1, 4, 3, 3));
        lorries.setPreferredSize(new Dimension(1270, 120));
        lorries.setBackground(Color.yellow);
        lorries.setBorder(BorderFactory.createEmptyBorder(6, 6, 3, 6));
        
        cars = new JPanel();
        cars.setLayout(new GridLayout(3, 4, 134, 3));
        cars.setPreferredSize(new Dimension(1270, 360));
        cars.setBackground(Color.yellow);
        cars.setBorder(BorderFactory.createEmptyBorder(3, 67, 6, 67));
        
        space = new JLabel[lSpaces + sSpaces];
        
        for (int l = 0; l < (lSpaces + sSpaces); l++)
        {
            if (l < lSpaces)
            {
                space[l] = new JLabel(new ImageIcon(sNums[l]));
                space[l].setPreferredSize(new Dimension(314, 115));
                
                lorries.add(space[l]);
            }
            else
            {
                space[l] = new JLabel(new ImageIcon(sNums[l]));
                space[l].setPreferredSize(new Dimension(157, 115));
                
                cars.add(space[l]);
            }
            
            space[l].addMouseListener(this);
            space[l].setBackground(Color.black);
            space[l].setOpaque(true);
            
            theSpaces.add(lorries);
            theSpaces.add(cars);
        }
    }
    
    
    /**
     * Method to select correct space type when updating image.
     */
    void resetImages()
    {
        for (int l = 0; l < (lSpaces + sSpaces); l++)
        {
            if (l < lSpaces)
            {
                setImage(l);
                lorries.add(space[l]);
            }
            else
            {
                setImage(l);
                cars.add(space[l]);
            }
        }
    }
    
    
    /**
     * Method to fetch the correct image when updating the GUI.
     * 
     * @param l Space to get the image for(0 base).
     */
    private void setImage(int l)
    {
        if (cPark.spaceFull(l))
        {
            space[l].setIcon(cPark.fetchImage(l));
            space[l].setBackground(Color.white);
        }
        else
        {
            space[l].setIcon(new ImageIcon(sNums[l]));
            space[l].setBackground(Color.black);
        }
    }
    
    
    /**
     * Method to delegate actions initiated from one of the buttons on the menu.
     * 
     * @param ae    ActionEvent object.
     */
    @Override
    public void actionPerformed(ActionEvent ae)
    {
        for (int b = 0; b < 8; b++)
        {
            if (ae.getSource().equals(command[b]))
            {
                ButtonAction ba = new ButtonAction(cPark, b, this);
                
                ba = null;
            }
        }
    }
    
    
    /**
     * Method to delegate actions initiated from a mouse button click on one of the spaces.
     * 
     * @param me    MouseEvent object.
     */
    @Override
    public void mouseClicked(MouseEvent me)
    {
        for (int s = 0; s < (lSpaces + sSpaces); s++)
        {
            if (me.getSource().equals(space[s]))
            {
                for (int c = 1; c < 4; c++)
                {
                    if (me.getButton() == c)
                    {
                        ButtonAction ba = new ButtonAction(cPark, s, c, this);
                        
                        ba = null;
                    }
                }
            }
        }
    }
    
    
    @Override
    public void mousePressed(MouseEvent me)
    {
        // NO ACTION
    }
    
    @Override
    public void mouseReleased(MouseEvent me)
    {
        // NO ACTION
    }
    
    @Override
    public void mouseEntered(MouseEvent me)
    {
        // NO ACTION
    }
    
    @Override
    public void mouseExited(MouseEvent me)
    {
        // NO ACTION
    }
}
