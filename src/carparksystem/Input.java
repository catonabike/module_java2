/*
 * Input.java
 */
package carparksystem;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * Helper class to build the additional GUI for data input to the Car Park System.
 * 
 * @author James Hudson
 */
class Input extends JPanel implements ActionListener, ItemListener
{
    private ButtonAction controller;
    private Object[] infoRef;
    private String type;
    private JTextField regIn, textIn;
    private JButton okIn, cancelIn;
    private JCheckBox checkIn;
    private JComboBox listIn;
    private JLabel fieldOne, fieldTwo, fieldThree, fieldFour;
    private JPanel descript, entry;
    private Border spacerA, spacerB, spacerC;
    
    
    /**
     * Default Constructor.
     */
    Input()
    {}
    
    
    /**
     * Constructor to get details of a new vehicle.
     * 
     * @param s Vehicle type.
     * @param b ButtonAction class.
     */
    Input(String s, ButtonAction b)
    {
        controller = b;
        type = s;
        infoRef = new Object[4];
        
        build();
    }
    
    
    /**
     * Constructor to edit the details of an existing vehicle.
     * 
     * @param s Vehicle type.
     * @param o Current details of vehicle.
     * @param b ButtonAction class.
     */
    Input(String s, Object[] o, ButtonAction b)
    {
        controller = b;
        infoRef = o;
        type = s;
        
        build();
    }
    
    
    /**
     * Method to build the GUI.
     */
    private void build()
    {
        descript = new JPanel();
        descript.setLayout(new BoxLayout(descript, BoxLayout.PAGE_AXIS));
        descript.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 0));
        
        entry = new JPanel();
        entry.setLayout(new BoxLayout(entry, BoxLayout.PAGE_AXIS));
        entry.setBorder(BorderFactory.createEmptyBorder(10, 60, 10, 75));
        
        spacerA = BorderFactory.createEmptyBorder(2, 0, 2, 0);
        spacerB = BorderFactory.createEmptyBorder(4, 0, 5, 0);
        spacerC = BorderFactory.createEmptyBorder(4, 0, 4, 0);
        
        buildFieldOne();
        buildFieldTwo();
        buildFieldThree();
        buildFieldFour();
        buildButtons();
        
        add(descript);
        add(entry);
    }
    
    
    /**
     * Method to set the first input field.
     */
    private void buildFieldOne()
    {
        fieldOne = new JLabel("Registration Number:");
        fieldOne.setAlignmentX(RIGHT_ALIGNMENT);
        fieldOne.setBorder(spacerA);
        
        regIn = new JTextField((String) infoRef[0], 8);
        regIn.setAlignmentX(LEFT_ALIGNMENT);
        regIn.addActionListener(this);
        
        descript.add(fieldOne);
        descript.add(Box.createRigidArea(new Dimension(0, 5)));
        entry.add(regIn);
        entry.add(Box.createRigidArea(new Dimension(0, 5)));
    }
    
    
    /**
     * Method to set the second input field.
     */
    private void buildFieldTwo()
    {
        if (type.equals("CAR"))
        {
            fieldTwo = new JLabel("Vehicle Length(metres):");
        }
        else if (type.equals("LORRY"))
        {
            fieldTwo = new JLabel("Vehicle Weight(tonnes):");
        }
        
        if (!type.equals("COACH"))
        {
            if (infoRef[1] != null) // for the edit vehicle instance
            {
                textIn = new JTextField("" + (Double) infoRef[1], 5);
            }
            else
            {
                textIn = new JTextField(5);
            }
            
            fieldTwo.setAlignmentX(RIGHT_ALIGNMENT);
            fieldTwo.setBorder(spacerA);
            
            textIn.setAlignmentX(LEFT_ALIGNMENT);
            textIn.addActionListener(this);
            
            descript.add(fieldTwo);
            entry.add(textIn);
        }
        
        descript.add(Box.createRigidArea(new Dimension(0, 5)));
        entry.add(Box.createRigidArea(new Dimension(0, 5)));
    }
    
    
    /**
     * Method to set the third input field.
     */
    private void buildFieldThree()
    {
        if (type.equals("CAR"))
        {
            fieldThree = new JLabel("Disabled Badge Holder:");
            
            if (infoRef[2] != null) // for the edit vehicle instance
            {
                checkIn = new JCheckBox("Yes", (Boolean) infoRef[2]);
            }
            else
            {
                checkIn = new JCheckBox("Yes", false);
                
                infoRef[2] = false;
            }
        }
        else if (type.equals("COACH"))
        {
            fieldThree = new JLabel("Tourist Operator:");
            
            if (infoRef[1] != null)
            {
                checkIn = new JCheckBox("Yes", (Boolean) infoRef[1]);
            }
            else
            {
                checkIn = new JCheckBox("Yes", false);
                
                infoRef[1] = false;
            }
        }
        
        if (!type.equals("LORRY"))
        {
            fieldThree.setAlignmentX(RIGHT_ALIGNMENT);
            fieldThree.setBorder(spacerC);
            
            checkIn.setAlignmentX(LEFT_ALIGNMENT);
            checkIn.addItemListener(this);
            
            descript.add(fieldThree);
            entry.add(checkIn);
        }
        
        descript.add(Box.createRigidArea(new Dimension(0, 5)));
        entry.add(Box.createRigidArea(new Dimension(0, 5)));
    }
    
    
    /**
     * Method to set the fourth input field.
     */
    private void buildFieldFour()
    {
        switch (type)
        {
            case "CAR":
                fieldFour = new JLabel("Number of Hours:");
                Integer hours[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19};
                listIn = new JComboBox<>(hours);
                if (infoRef[3] != null) // for the edit vehicle instance
                    listIn.setSelectedIndex((Integer) infoRef[3] - 1);
                break;
            case "LORRY":
                fieldFour = new JLabel("Number of Days:");
                Integer days[] = {1, 2, 3, 4, 5, 6, 7};
                listIn = new JComboBox<>(days);
                if (infoRef[2] != null)
                    listIn.setSelectedIndex((Integer) infoRef[2] - 1);
                break;
            default:
                fieldFour = new JLabel("Number of seats:");
                String seats[] = {"up to 20", "over 20"};
                listIn = new JComboBox<>(seats);
                if (infoRef[2] != null)
                    listIn.setSelectedIndex((Integer) infoRef[2]);
                break;
        }
        
        fieldFour.setAlignmentX(RIGHT_ALIGNMENT);
        fieldFour.setBorder(spacerB);
        
        listIn.setAlignmentX(LEFT_ALIGNMENT);
        listIn.addActionListener(this);
        
        descript.add(fieldFour);
        descript.add(Box.createRigidArea(new Dimension(0, 10)));
        entry.add(listIn);
        entry.add(Box.createRigidArea(new Dimension(0, 10)));
    }
    
    
    /**
     * Method to add the buttons of the GUI.
     */
    private void buildButtons()
    {
        okIn = new JButton("OK");
        okIn.setAlignmentX(RIGHT_ALIGNMENT);
        okIn.addActionListener(this);
        
        cancelIn = new JButton("Cancel");
        cancelIn.setAlignmentX(LEFT_ALIGNMENT);
        cancelIn.addActionListener(this);
        
        descript.add(okIn);
        entry.add(cancelIn);
    }
    
    
    /**
     * Method to set vehicle details and close the GUI based on a button action.
     * 
     * @param ae    ActionEvent object.
     * 
     * @exception   NumberFormatException on parsing double from input field.
     */
    @Override
    public void actionPerformed(ActionEvent ae)
    {
        if (ae.getSource().equals(okIn))
        {
            switch (type)
            {
                case "CAR":
                    infoRef[0] = regIn.getText();
                    
                    try
                    {
                        infoRef[1] = Double.parseDouble(textIn.getText());
                    }
                    catch (NumberFormatException nfe)
                    {
                        infoRef[1] = 99.0; // sets field to 99.0 which will cause validation to fail.
                    }
                    
                    infoRef[3] = listIn.getSelectedItem();
                    break;
                case "LORRY":
                    infoRef[0] = regIn.getText();
                    
                    try
                    {
                        infoRef[1] = Double.parseDouble(textIn.getText());
                    }
                    catch (NumberFormatException nfe)
                    {
                        infoRef[1] = 99.0;
                    }
                    
                    infoRef[2] = listIn.getSelectedItem();
                    break;
                default:
                    infoRef[0] = regIn.getText();
                    infoRef[2] = listIn.getSelectedIndex();
            }
            
            controller.setNewData(type, infoRef);
            controller.closeInput();
        }
        
        if (ae.getSource().equals(cancelIn))
            controller.closeInput();
    }
    
    
    /**
     * Method to update data fields using the JCheckBox objects when the state changes.
     * 
     * @param ie ItemEvent object.
     */
    @Override
    public void itemStateChanged(ItemEvent ie)
    {
        int r = 1;
        
        if (type.equals("CAR"))
            r = 2;
        
        infoRef[r] = ie.getStateChange() == ItemEvent.SELECTED;
    }
}
