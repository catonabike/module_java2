/*
 * CarParkFile.java
 */
package carparksystem;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.io.File;

/**
 * Class to serialise or de-serialise the car park data.
 * 
 * @author James Hudson
 */
public class CarParkFile
{
    private String fileName;
    private HashMap<Integer, Vehicle> toProcess = null;
    
    
    /**
     * Default Constructor.
     */
    CarParkFile()
    {}
    
    
    /**
     * Constructor to serialise or de-serialise the car park.
     * 
     * @param name  Filename top write to or read from.
     * @param map   Car park.
     */
    CarParkFile(String name, HashMap<Integer, Vehicle> map)
    {
        fileName = name;
        toProcess = map;
    }
    
    
    /**
     * Method to validate the passed file name.
     * 
     * @return  true if validation passes.
     */
    public boolean testName()
    {
        return (fileName.length() > 4) && (fileName.substring(fileName.length() - 4).equals(".dat"));
    }
    
    
    /**
     * Method to serialise the object to a data file.
     * 
     * @return  true if no exceptions are created.
     * 
     * @throws java.io.IOException if the specified file can not be created or opened or written to.
     */
    public boolean savePark() throws IOException
    {
        boolean success = false;
        
        if (testName())
        {
            File f = new File(fileName);
            
            f.createNewFile();
            
            ObjectOutputStream output = new ObjectOutputStream(new FileOutputStream(fileName));
            
            output.writeObject(toProcess);
                
            success = true;
        }
        return success;
    }
    
    
    /**
     * Method to de-serialise the object from a data file and  send it back to the car park.
     * 
     * @return An instance of the car park or null if there is an exception.
     * 
     * @throws java.lang.ClassNotFoundException if the class of the serialised object is not specified.
     * @throws java.io.IOException if the specified file can not be opened or read from.
     */
    @SuppressWarnings("unchecked") // to prevent warnings on casting the read Object to a HashMap, as sure of the class.
    HashMap<Integer, Vehicle> loadPark() throws ClassNotFoundException, IOException
    {
        if (testName())
        {
            ObjectInputStream input = new ObjectInputStream(new FileInputStream(fileName));
            
            toProcess = (HashMap<Integer, Vehicle>) input.readObject();
        }
        return toProcess;
    }   
}