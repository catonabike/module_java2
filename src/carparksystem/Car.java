/*
 * Car.java
 */
package carparksystem;

import javax.swing.ImageIcon;

/**
 *Vehicle subclass Car, to store specific information and methods.
 * 
 * @author James Hudson
 */
public class Car extends Vehicle
{
    private double length;
    private boolean disabledBadge;
    private int numOfHours;
    
    
    /**
     * Default Constructor.
     */
    Car()
    {
        super();
        
        length = 0.0;
        disabledBadge = false;
        numOfHours = 0;
    }
    
    
    /**
     * Constructor taking new car details.
     * 
     * @param rNo   Registration number.
     * @param len   Car length.
     * @param dis   Disabled badge holder status.
     * @param hr    Parking time.
     */
    public Car(String rNo, double len, boolean dis, int hr)
    {
        super(rNo);
        
        setLength(len);
        
        disabledBadge = dis;
        numOfHours = hr;
        
        calcCharge();
    }
    
    
    /**
     * Method to return the car length.
     * 
     * @return  the length in meters as a double.
     */
    public double getLength()
    {
        return length;
    }
    
    
    /**
     * Method to set the car length.
     * 
     * @param l Car length.
     */
    public void setLength(double l)
    {
        length = l;
        
        if (length < 2)
            length = 2.0;
    }
    
    
    /**
     * Method to get the disabled badge holder status.
     * 
     * @return  true if a disabled badge holder.
     */
    public boolean getDisabledBadge()
    {
        return disabledBadge;
    }
    
    
    /**
     * Method to return the length of the stay in the car park.
     * 
     * @return  Time in hours as an integer.
     */
    public int getNumOfHours()
    {
        return numOfHours;
    }
    
    
    /**
     * Method to set the length of the stay in the car park.
     * 
     * @param h Number of hours.
     */
    public void setNumOfHours(int h)
    {
        numOfHours = h;
    }
    
    
    /**
     * Method to return the cars details.
     * 
     * @return  an object array containing the details.
     */
    @Override
    public Object[] getCurrentData()
    {
        Object[] temp = {super.getRegNumber(), length, disabledBadge, numOfHours};
        
        return temp;
    }
    
    
    /**
     * Method to return the vehicle type.
     * 
     * @return  Car as a string.
     */
    @Override
    public String getType()
    {
        return "CAR";
    }
    
    
    /**
     * Method to test if the car length is under the limit.
     * 
     * @return  true if less then or equal to 10 meters.
     */
    @Override
    public boolean testLimit()
    {
        return length <= 10;
    }
    
    
    /**
     * Method to get an instance of a car image.
     * 
     * @return  ImageIcon instance containing the image to show on the GUI.
     */
    @Override
    public ImageIcon getImage()
    {
        if (length <= 6)
            return (new ImageIcon("ParkingImages/acar.jpg"));
        else
            return (new ImageIcon("ParkingImages/acamper.jpg"));
    }
    
    
    /**
     * Method to calculate the parking charge.
     */
    @Override
    public void calcCharge()
    {
        if (disabledBadge)
        {
            charge = 0.0;
        }
        else
        {
            if (length <= 6.0)
                charge = (1 * numOfHours);
            else
                charge = (1.5 * numOfHours);
        }
    }
    
    
    /**
     * Method to return the details of the car.
     * 
     * @return  the description as a string.
     */
    @Override
    public String toString()
    {
        return ("Car\n" + super.toString() + "\nLength: " + String.format("%.2f", length) + " metres.\nStay duration: " + numOfHours + " hours.");
    }
}